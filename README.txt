Thank you for your interest in our basketball website.

Here are a few short steps to get you started...


0. Prerequisite
===============

Visual studio requires .net framework 4.6.1 or higher
Python 3.x.x, requires java 1.8
Check http://www.oracle.com/technetwork/java/javase/downloads/index.html to download the JDK.


1. Installing VisualStudio and Python
=====================================

Installing VisualStudio and Python is just a matter of downloading it from their respective sites and double click on the .exe or .msi files and follow the on screen instruction.And further please refer to the installation docs that are bundled in this package.


The default domain called 'domain1' is installed and preconfigured.


2. Starting Visual Studio and Python
=====================================


To start Visual Studio, just go in the directory where Visual Studio/Python is located and type:
        
        On Windows for python to run: in command line enter “python script.name”
	On Windows for VisualStudio to run : enter on the visual studio icon to start the IDE.

After a few seconds, Visual Studio will be up and ready to start project. The default 'domain1' domain is configured to listen on port 8080. In your browser, go to http://localhost:8080 to see the default landing page.




3. Stopping Visual Studio services 
==========================

To stop Visual studio service, just close the visual studio IDE.
       


4. Where to go next?
====================

Open the following local file in word: UserManual.docx. It contains useful information such as the details related to how to navigate and purpose of the website etc.,


5. Documentation 
================

Visual studio 2105 documentation : https://www.visualstudio.com/en-us/docs/vs/overview


Python 3.x.x Documentation : https://docs.python.org/3/

C# Documentation :https://msdn.microsoft.com/en-us/library/kx37x362.aspx




6. Follow us
============

Make sure to follow us at northwestbearcat.com

