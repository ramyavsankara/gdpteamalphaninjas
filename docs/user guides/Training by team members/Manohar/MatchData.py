from bs4 import BeautifulSoup
import urllib
import os
toOpenUrl=urllib.urlopen("http://bearcatsports.com/cumestats.aspx?path=mbball&year=2015")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find('table',{"class":"stats_table center_wide has_cell_borders has_vert_padding"})
table_Body=tables.tbody
dataCmptly=""
for rows in table_Body.findAll('tr'):
    cellInRow="22015"
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text
    dataCmptly=dataCmptly+"\n"+cellInRow[1:]

print(dataCmptly)
header = "Year,Opponent,DatePlayed,Score,Mar,FieldGoals,%fieldGoals,3fgmade,%3fgmade,ftm/attempted,%ftm/attempted,Rebounds,Assist,turnovers,block,steal,PF"+"\n"
file = open(os.path.expanduser("MatchData.csv"), "wb")
file.write(bytes(header))
file.write(bytes(dataCmptly))



print("new 2014")
toOpenUrl_2014=urllib.urlopen("http://bearcatsports.com/cumestats.aspx?path=mbball&year=2014")
urlLink_2014=BeautifulSoup(toOpenUrl, "html.parser")
tables_2014=urlLink.find('table',{"class":"stats_table center_wide has_cell_borders has_vert_padding"})
table_Body_2014=tables_2014.tbody
dataCmptly_2014=""
for rows_2014 in table_Body_2014.findAll('tr'):
    cellInRow_2014="22014"
    for cells_2014 in rows_2014.findAll('td'):
        cellInRow_2014=cellInRow_2014+","+cells_2014.text
    dataCmptly_2014=dataCmptly_2014+"\n"+cellInRow_2014[1:]

print(dataCmptly_2014)
file_2014 = open(os.path.expanduser("MatchData.csv"), "a")
file_2014.write(bytes(dataCmptly_2014))


print("new 2013")
toOpenUrl_2013=urllib.urlopen("http://bearcatsports.com/cumestats.aspx?path=mbball&year=2013")
urlLink_2013=BeautifulSoup(toOpenUrl, "html.parser")
tables_2013=urlLink.find('table',{"class":"stats_table center_wide has_cell_borders has_vert_padding"})
table_Body_2013=tables_2013.tbody
dataCmptly_2013=""
for rows_2013 in table_Body_2013.findAll('tr'):
    cellInRow_2013="22013"
    for cells_2013 in rows_2013.findAll('td'):
        cellInRow_2013=cellInRow_2013+","+cells_2013.text
    dataCmptly_2013=dataCmptly_2013+"\n"+cellInRow_2013[1:]

print(dataCmptly_2013)
file_2013 = open(os.path.expanduser("MatchData.csv"), "a")
file_2013.write(bytes(dataCmptly_2013))
