﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using NWSports_BasketBall.Model;
using Xunit;
using NWSports_BasketBall.Models;
using NWSports_BasketBall.Controllers;
using Microsoft.AspNetCore.Mvc;
using NWSports_BasketBall.Data;
using Microsoft.EntityFrameworkCore.Infrastructure;

using Microsoft.EntityFrameworkCore;

namespace NWSports_BasketBall.Model.Test
{
    public class HomeControllerTest
    {

        public class Class1
        {
            [Fact]
            public void PassingTest()
            {
                Assert.Equal(4, Add(2, 2));
            }

            [Fact]
            public void FailingTest()
            {
                Assert.Equal(4, Add(2, 2));
            }

            int Add(int x, int y)
            {
                return x + y;
            }
        }
        private readonly IServiceProvider _serviceProvider;
        public HomeControllerTest()
        {
            var efServiceProvider = new Microsoft.Extensions.DependencyInjection.ServiceCollection();
            var services = new Microsoft.Extensions.DependencyInjection.ServiceCollection();
       //     services.AddEntityFramework()
       //.AddInMemoryDatabase()
       //.AddDbContext<ApplicationDbContext>(
       //options => options.UseInMemoryDatabase());
       //     _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void About_CreatesView()
        {
            var controller = new HomeController();
            var result = controller.About(); // controller uses sync methods (not async)

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);

        }

        [Fact]
        public void Contact_CreatesViewWithMessage()
        {
            var controller = new HomeController();
            var result = controller.Contact(); // controller uses sync methods (not async)
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
            Assert.Same("Your contact page.", viewResult.ViewData["Message"]);
        }

        [Fact]
        public void Error_CreatesViewWithMessage()
        {
            var controller = new HomeController();
            var result = controller.Error(); // controller uses sync methods (not async)
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        [Fact]
        public void Index_CreatesViewWithMessage()
        {
            var controller = new HomeController();
            var result = controller.Index(); // controller uses sync methods (not async)

            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        [Fact]
        public void Home_CreatesView()
        {
            var controller = new HomeController();
            var result = controller.Index(); // controller uses sync methods (not async)
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
        [Fact]
        public void Home_ReturnView()
        {
            var controller = new HomeController();
            var result = controller.Index(); // controller uses sync methods (not async)
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }
    }
}