from bs4 import BeautifulSoup
import urllib
import os
toOpenUrl=urllib.urlopen("http://bearcatsports.com/cumestats.aspx?path=mbball&year=2015")
urlLink=BeautifulSoup(toOpenUrl, "html.parser")
tables=urlLink.find('table',{"class":"has_sorting stats_table center_wide has_cell_borders has_vert_padding"})
table_Body=tables.tbody
dataCmptly=""
for rows in table_Body.findAll('tr'):
    cellInRow=""
    for cells in rows.findAll('td'):
        cellInRow=cellInRow+","+cells.text
    dataCmptly=dataCmptly+"\n"+cellInRow[1:]

print(dataCmptly)
header = "Number,FirstName,LastName,gp,gs,tot,avg,fg,fga,ga%,3fg,3fga,3pt%,ft,fta,ft%,off,def,rtot,ravg,pf,a,ast/g,to,st,bl,pts,avg"+"\n"
file = open(os.path.expanduser("PlayersData.csv"), "wb")
file.write(bytes(header))
file.write(bytes(dataCmptly))


