﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class Team
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TeamID { get; set; }
        public string TeamName { get; set; }
       public string HomeCourt { get; set; }
        public static List<Team> ReadAllFromCsv(string filepath)
        {
            List<Team> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Team.OneFromCsv(v))
                                        .ToList();
            return lst;
        }



        public static Team OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Team item = new Team();

            int i = 0;
            item.TeamID = Convert.ToInt32(values[i++]);
            item.TeamName = Convert.ToString(values[i++]);
           item.HomeCourt = Convert.ToString(values[i++]);



            return item;
        }
    }

}
