﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class Player
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlayerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double Height { get; set; }
        public string Weight { get; set; }

        //[ScaffoldColumn(true)]
        //[Display(Name = "Position ID")]
        //public int PositionID { get; set; }
        //public Position position { get; set; }
        public static List<Player> ReadAllFromCsv(string filepath)
        {
            List<Player> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Player.OneFromCsv(v))
                                        .ToList();
            return lst;
        }



        public static Player OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Player item = new Player();

            int i = 0;
            Console.WriteLine("Reading value " + values[i]);
            item.PlayerID = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.FirstName = Convert.ToString(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.LastName = Convert.ToString(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Height = Convert.ToDouble(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Weight = Convert.ToString(values[i++]);

            //Console.WriteLine("Reading value " + values[i]);
            //item.PositionID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
