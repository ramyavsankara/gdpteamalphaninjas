﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class Performance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PerformanceID { get; set; }
        public double Assists { get; set; }
        public double Points { get; set; }
        public double Rebounds { get; set; }
         public double Steals { get; set; }
        //public int PlayerID { get; set; }
        //public Player player { get; set; }
        //public int PositionID { get; set; }
        //public Position position1 { get; set; }
        //public int MatchID { get; set; }
        //public Match Match { get; set; }
        public static List<Performance> ReadAllFromCsv(string filepath)
        {
            List<Performance> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Performance.OneFromCsv(v))
                                        .ToList();
            return lst;
        }



        public static Performance OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Performance item = new Performance();

            int i = 0;
            Console.WriteLine("Reading value " + values[i]);
            item.PerformanceID = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Assists = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Points = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Rebounds = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.Steals = Convert.ToInt32(values[i++]);

            //Console.WriteLine("Reading value " + values[i]);
            //item.PositionID = Convert.ToInt32(values[i++]);

            //Console.WriteLine("Reading value " + values[i]);
            //item.MatchID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
