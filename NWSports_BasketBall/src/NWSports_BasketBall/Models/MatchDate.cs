﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class MatchDate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MatchDateID { get; set; }
        public int Date { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        //public int TeamID { get; set; }
        //public Team team1 { get; set; }


        public static List<MatchDate> ReadAllFromCsv(string filepath)
        {
            List<MatchDate> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => MatchDate.OneFromCsv(v))
                                        .ToList();
            return lst;
        }



        public static MatchDate OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            MatchDate item = new MatchDate();

            int i = 0;
            item.MatchDateID = Convert.ToInt32(values[i++]);
            item.Date = Convert.ToInt32(values[i++]);
            item.Month = Convert.ToInt32(values[i++]);
            item.Year = Convert.ToInt32(values[i++]);
            //item.TeamID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
