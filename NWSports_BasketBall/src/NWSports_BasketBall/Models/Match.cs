﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class Match
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MatchID { get; set; }
        public double HomePoints { get; set; }
        public double OpponentPoints { get; set; }

        //[ScaffoldColumn(true)]
        //[Display(Name = "Team ID")]
        //public int TeamID { get; set; }
        //public Team team { get; set; }

        //[ScaffoldColumn(true)]
        //[Display(Name = "Date ID")]
        //public int MatchDateID { get; set; }
        //public MatchDate matchdate { get; set; }
        public static List<Match> ReadAllFromCsv(string filepath)
        {
            List<Match> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Match.OneFromCsv(v))
                                        .ToList();
            return lst;
        }



        public static Match OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Match item = new Match();

            int i = 0;
            Console.WriteLine("Reading value " + values[i]);
            item.MatchID = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.HomePoints = Convert.ToInt32(values[i++]);

            Console.WriteLine("Reading value " + values[i]);
            item.OpponentPoints = Convert.ToInt32(values[i++]);

            //Console.WriteLine("Reading value " + values[i]);
            //item.TeamID = Convert.ToInt32(values[i++]);

            //Console.WriteLine("Reading value " + values[i]);
            //item.MatchDateID = Convert.ToInt32(values[i++]);


            return item;
        }
    }

}

