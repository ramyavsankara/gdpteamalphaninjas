﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NWSports_BasketBall.Models
{
    public class Position
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PositionID { get; set; }
        public string PositionCode { get; set; }
        public string PositionName { get; set; }
        public static List<Position> ReadAllFromCsv(string filepath)
        {
            List<Position> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Position.OneFromCsv(v))
                                        .ToList();
            return lst;
        }


        public static Position OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Position item = new Position();

            int i = 0;
            item.PositionID = Convert.ToInt32(values[i++]);
            item.PositionCode = Convert.ToString(values[i++]);
            item.PositionName = Convert.ToString(values[i++]);
           


            return item;
        }
    }
}
