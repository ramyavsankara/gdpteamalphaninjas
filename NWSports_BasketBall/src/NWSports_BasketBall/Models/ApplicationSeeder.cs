﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NWSports_BasketBall.Data;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace NWSports_BasketBall.Models
{
    public class ApplicationSeeder : Controller
    {
        public static void EnsureSeedData(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();

            // Perform complete database delete and create (not migrations)
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            // Seed data, basic entities first
            context.Player.AddRange(Player.ReadAllFromCsv(@"SeedData/Player.csv").ToArray());
            context.SaveChanges();

            context.Match.AddRange(Match.ReadAllFromCsv(@"SeedData/Match.csv").ToArray());
            context.SaveChanges();

            context.Position.AddRange(Position.ReadAllFromCsv(@"SeedData/Position.csv").ToArray());
            context.SaveChanges();

            context.Team.AddRange(Team.ReadAllFromCsv(@"SeedData/Team.csv").ToArray());
            context.SaveChanges();

            context.Performance.AddRange(Performance.ReadAllFromCsv(@"SeedData/Performance.csv").ToArray());
            context.SaveChanges();

            context.MatchDate.AddRange(MatchDate.ReadAllFromCsv(@"SeedData/MatchDate.csv").ToArray());
            context.SaveChanges();

            // release resources
            context.Dispose();
        }

    }
}
