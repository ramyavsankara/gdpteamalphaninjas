using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NWSports_BasketBall.Data;
using NWSports_BasketBall.Models;

namespace NWSports_BasketBall.Controllers
{
    public class MatchDatesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MatchDatesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: MatchDates
        public async Task<IActionResult> Index()
        {
            return View(await _context.MatchDate.ToListAsync());
        }

        // GET: MatchDates/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            //var matchDate = await _context.MatchDate.SingleOrDefaultAsync(m => m.MatchDateID == id);
            //if (matchDate == null)
            //{
            //    return NotFound();
            //}

            return View();
        }

        // GET: MatchDates/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MatchDates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MatchDateID,Date,Month,Year")] MatchDate matchDate)
        {
            if (ModelState.IsValid)
            {
                _context.Add(matchDate);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(matchDate);
        }

        // GET: MatchDates/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var matchDate = await _context.MatchDate.SingleOrDefaultAsync(m => m.MatchDateID == id);
            if (matchDate == null)
            {
                return NotFound();
            }
            return View(matchDate);
        }

        // POST: MatchDates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MatchDateID,Date,Month,Year")] MatchDate matchDate)
        {
            if (id != matchDate.MatchDateID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(matchDate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MatchDateExists(matchDate.MatchDateID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(matchDate);
        }

        // GET: MatchDates/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var matchDate = await _context.MatchDate.SingleOrDefaultAsync(m => m.MatchDateID == id);
            if (matchDate == null)
            {
                return NotFound();
            }

            return View(matchDate);
        }

        // POST: MatchDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var matchDate = await _context.MatchDate.SingleOrDefaultAsync(m => m.MatchDateID == id);
            _context.MatchDate.Remove(matchDate);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool MatchDateExists(int id)
        {
            return _context.MatchDate.Any(e => e.MatchDateID == id);
        }
    }
}
